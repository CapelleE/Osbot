package workers

import org.pircbotx.PircBotX

class ConsoleCommander(val bot: PircBotX)
  extends Runnable {
  private val availableModules = bot.getConfiguration.getListenerManager.getListeners
  private var running: Boolean = false
  private var lastCommand: String = ""

  override def run(): Unit = {
    running = true
    while (running) {
      lastCommand = scala.io.StdIn.readLine("Command: ")
      parseCommand()
    }
  }

  private def parseCommand(): Unit = {
    lastCommand = lastCommand.toLowerCase

    if (lastCommand.contentEquals("exit")) // EXIT
      stop()

    else if (lastCommand.startsWith("join")) {
      // JOIN
      val split = lastCommand.split(" ")
      if (split.length == 2) {
        val channel = split(1)
        bot.sendIRC().joinChannel(channel)
      }
      else if (split.length == 3) {
        val channel = split(1)
        val key = split(2)
        bot.sendIRC().joinChannel(channel, key)
      }
      else
        println("Invalid command, usage: join [#channel] [key (optional)]")
    }
    else if (lastCommand.startsWith("leave") || lastCommand.startsWith("part")) {
      // LEAVE / PART
      val split = lastCommand.split(" ")

      if (split.length == 2) {
        val channel = split(1)
        bot.sendRaw().rawLineNow("PART %s".format(channel))
      }
      else {
        println("Invalid command, usage: part [#channel] OR leave [#channel]")
      }
    }
    else if (lastCommand.startsWith("kstats")) {

    }
  }

  def stop(): Unit = {
    running = false
    bot.sendIRC().quitServer()
  }
}
