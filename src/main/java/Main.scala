import modules.{AdminModule, KappaCounterModule}
import org.apache.log4j.BasicConfigurator
import org.pircbotx.{Configuration, PircBotX}
import workers.ConsoleCommander

object Main {
  def main(args: Array[String]): Unit = {
    BasicConfigurator.configure()
    val config = new Configuration.Builder()
      .setName("ORPAC")
      .setLogin("ORPAC")
      .setServerPassword("oauth:g1qfjj4xge7y42tnrfnh65jcnbwox8")
      .setServerHostname("irc.twitch.tv")
      .setServerPort(6667)
      .addListener(new AdminModule)
      .addListener(new KappaCounterModule)
      .addAutoJoinChannel("#orpac")
      .buildConfiguration
    val bot: PircBotX = new PircBotX(config)
    val commanderThread: Thread = new Thread(new ConsoleCommander(bot))
    commanderThread.start()
    bot.startBot()
    commanderThread.join()
  }
}
