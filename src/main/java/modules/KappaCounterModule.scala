package modules

import org.joda.time.{DateTime, Interval}
import org.pircbotx.PircBotX
import org.pircbotx.hooks.ListenerAdapter
import org.pircbotx.hooks.events.MessageEvent

import scala.collection.mutable

class KappaCounterModule
  extends ListenerAdapter[PircBotX] {
  private val start: DateTime = new DateTime()
  private val kappas: Array[String] = new Array[String](3)
  private var kappaMap: mutable.Map[String, Long] = scala.collection.mutable.Map[String, Long]()
  kappas(0) = "Kappa"
  kappas(1) = "Keepo"
  kappas(2) = "KappaPride"

  override def onMessage(event: MessageEvent[PircBotX]): Unit = {
    val words = event.getMessage.split(" ")
    words.foreach(w => {
      if (kappas.contains(w)) {
        incrementKappa(event.getChannel.getName)
      }
    })

    if (event.getMessage.toLowerCase.contentEquals("!kowner")) {
      event.respond("My owner is Oscuro87")
    }
    else if (event.getMessage.toLowerCase.startsWith("!kstats")) {
      try {
        val channel: String = event.getMessage.split(" ")(1)
        val now: DateTime = new DateTime()
        val interval: Interval = new Interval(start, now)
        val seconds = interval.toDuration.getStandardSeconds % 60
        val minutes = interval.toDuration.getStandardMinutes % 60
        val hours = interval.toDuration.getStandardHours % 24
        val days = interval.toDuration.getStandardDays
        val kps: Float = getKappaForChannel(channel).toFloat / interval.toDuration.getStandardSeconds.toFloat * 60.0f
        event.getBot.sendIRC().message(event.getChannel.getName, "# of Kappa: %d in %dd%dh%dm%ds (avg. %2f Kappas per minute) in channel %s".format(getKappaForChannel(channel), days, hours, minutes, seconds, kps, channel))
      }
      catch {
        case aioobe: ArrayIndexOutOfBoundsException =>
      }
    }
    else if (event.getMessage.toLowerCase.contentEquals("!reset")) {
      resetAll()
    }
  }

  private def incrementKappa(channel: String): Unit = {
    if (!kappaMap.contains(channel))
      kappaMap += channel -> 1
    else
      kappaMap(channel) += 1
  }

  private def getKappaForChannel(channel: String): Long = {
    if (!kappaMap.contains(channel))
      0
    else
      kappaMap(channel)
  }

  private def resetAll(): Unit = {
    kappaMap.clear()
  }

  private def resetForChannel(channel: String): Unit = {
    kappaMap -= channel
  }
}