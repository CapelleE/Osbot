package modules

import org.pircbotx.PircBotX
import org.pircbotx.hooks.ListenerAdapter
import org.pircbotx.hooks.events.{JoinEvent, MessageEvent}

import scala.collection.mutable.ArrayBuffer

class AdminModule extends ListenerAdapter[PircBotX] {
  private val mainChannelName: String = "#orpac"
  private val whisperChannel: String = "#jtv"
  private val allowedUsers: ArrayBuffer[String] = new ArrayBuffer[String]()
  allowedUsers += "oscuro87"


  override def onJoin(event: JoinEvent[PircBotX]): Unit = {
    if (event.getChannel.getName == mainChannelName) {
      event.getBot.sendRaw().rawLineNow("CAP REQ :twitch.tv/commands")
      event.getBot.sendIRC().message(mainChannelName, "ORPAC bot is now online")
    }
  }

  override def onMessage(event: MessageEvent[PircBotX]): Unit = {
    if (event.getMessage.toLowerCase.startsWith("!join")) {
      if (!checkAllowedUser(event)) return
      val pieces: Array[String] = event.getMessage.split(" ")
      if (pieces.length == 2)
        event.getBot.sendIRC().joinChannel(pieces(1))
      else
        event.getBot.sendIRC().joinChannel(pieces(1), pieces(2))
      event.getBot.sendIRC().message(mainChannelName, "Joined channel %s".format(pieces(1)))
    }
    else if (event.getMessage.toLowerCase.startsWith("!part") || event.getMessage.toLowerCase.contains("!leave")) {
      if (!checkAllowedUser(event)) return
      val pieces: Array[String] = event.getMessage.split(" ")
      if (pieces.length == 2) {
        val channel: String = pieces(1)
        event.getBot.sendRaw().rawLine("PART %s".format(channel))
        event.getBot.sendIRC().message(mainChannelName, "Left channel %s".format(channel))
      }
    }
    else if (event.getMessage.toLowerCase.startsWith("!kallowuser")) {
      if (!checkAllowedUser(event)) return
      val users: Array[String] = event.getMessage.split(" ")
      if (users.length > 1) {
        users.drop(0)
        users.foreach(usr => {
          allowedUsers += usr
          println("Allowed user %s as admin.".format(usr))
          event.getBot.sendIRC().message(mainChannelName, "Allowed user %s as admin.".format(usr))
        })
      }
    }
  }

  private def checkAllowedUser(event: MessageEvent[PircBotX]): Boolean = {
    var allowed = false
    allowedUsers.foreach(au => {
      if (au.equalsIgnoreCase(event.getUser.getNick))
        return true
    })
    event.getBot.sendIRC().message(mainChannelName, "You are not allowed to use this command. (%s, %s, %s)".format(event.getMessage, event.getUser.getNick, event.getChannel.getName))
    event.getBot.sendIRC().message(event.getChannel.getName, "You are not allowed to use this command.")
    false
  }
}
