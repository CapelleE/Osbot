package modules

import models.betting.PlayerInfo
import org.pircbotx.PircBotX
import org.pircbotx.hooks.ListenerAdapter
import org.pircbotx.hooks.events.{DisconnectEvent, MessageEvent, QuitEvent}

/**
 * Betting module for the bot.
 * Stores players data into an xml file.
 */
class BettingModule extends ListenerAdapter[PircBotX] {
  private val betsXML = scala.xml.XML.loadFile("players.xml")
  // Located in the src/main/resources/ folder
  private val basePlayerMoney = 250.0f

  override def onMessage(event: MessageEvent[PircBotX]): Unit = {

  }

  override def onDisconnect(event: DisconnectEvent[PircBotX]): Unit = {

  }

  override def onQuit(event: QuitEvent[PircBotX]): Unit = {

  }

  private def registerPlayer(playerName: String): Unit = {
    //TODO registerPlayer
    // Check if player exists first
    // Unicity = irc nicknames must be unique (user can change though)
  }

  private def addNewBet(event: MessageEvent[PircBotX]): Unit = {
    //TODO
  }

  private def readPlayerInfos(playerName: String): PlayerInfo = {
    //TODO
    val result: PlayerInfo = new PlayerInfo()
    if (!doesPlayerExist(playerName))
      throw new Exception("This player doesn't exist")
    null
  }

  private def doesPlayerExist(playerName: String): Boolean = {
    //TODO doesPlayerExist
    false
  }

  private def writePlayerInfos(infos: PlayerInfo, newEntry: Boolean): Unit = {
    //TODO
  }
}
