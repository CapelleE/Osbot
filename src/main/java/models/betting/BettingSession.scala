package models.betting

import scala.collection.mutable.ArrayBuffer

class BettingSession(val bettingReason: String,
                     val availableOptions: Array[BetOption],
                     val maxEntrants: Int = -1) {
  val entrants: ArrayBuffer[Bet] = new ArrayBuffer[Bet](10)

  def getTotalEntrants: Int = entrants.size

  def getTotalBetsAmount: Float = {
    var acc: Float = 0.0f
    entrants.foreach(e => {
      acc += e.betAmount
    })
    acc
  }

  def getBetsDistribution: Array[String] = {
    val result: Array[String] = new Array[String](availableOptions.length)
    // TODO
    result
  }

  def addBet(playerName: String, betOption: String, amount: Float): Boolean = {
    if (hasPlayerAlreadyEntered(playerName))
      return false

    if (amount <= 1.0f) //TODO || amount > betterMoney)
      return false

    val betOptionChosen: BetOption = stringToBetOption(betOption)
    val newBet: Bet = new Bet(playerName, betOptionChosen, amount)
    true
  }

  def hasPlayerAlreadyEntered(playerNameToCheck: String): Boolean = {
    entrants.foreach(e => {
      if (e.playerName.contentEquals(playerNameToCheck)) return true
    })
    false
  }

  private def stringToBetOption(optionStr: String): BetOption = {
    availableOptions.foreach(opt => {
      if (opt.equals(optionStr))
        return opt
    })
    null
  }
}
