package models.betting

class BetOption(val optionName: String) {
  var totalBets: Float = 0.0f

  def bet(amount: Float): Unit = totalBets += amount

  override def equals(other: scala.Any): Boolean = {
    other match {
      case a: BetOption =>
        this == other
      case b: String =>
        this.optionName.contentEquals(other.asInstanceOf[String])
    }
  }
}
