package models.betting

class Bet(val playerName: String,
          val betOption: BetOption,
          val betAmount: Float) {}
